public class MethodOLJava {
    void display(int a) {
        System.out.println("I am integer");
    }

    void display(char c) {
        System.out.println("I am character");
    }

    public static void main(String args[]) {
        MethodOLJava obj = new MethodOLJava();
        obj.display(3);
        obj.display('s');
    }
}
