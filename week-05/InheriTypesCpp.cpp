#include<iostream>
using namespace std;
class parent1
{
    public:
    parent1()
    {
        cout<<"i read news paper daily"<<endl;
    }
};
class parent2
{
    public:
    parent2()
    {
        cout<<"i am a workohlic "<< endl;
    }
};
class child1:public parent1 //simple inheritance
{
    public:
    child1()
    {
        cout<<"i am the younger one of the family"<<endl;
    }
};
class child2:public parent1,public parent2 //multiple inheritance
{
    public:
};
class child3:public child1//multilevel inheritance
{
    public:
};
class child4:public parent1 //hiracheal inheritance
{
    public:
};
class child5:public parent1 //hiracheal inheritance
{
    public:
};
int main()
{
    child1 obj1;
    child2 obj2;
    child3 obj3;
    child4 obj4;
    child5 obj5;
    return 0;
}